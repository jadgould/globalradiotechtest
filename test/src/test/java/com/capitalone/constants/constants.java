package com.capitalone.constants;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class constants {

	public final static String DEVICE_NAME = "Android Emulator";
	public final static String PLATFORM_VERSION = "5.1";
	public final static String PLATFORM_NAME = "Android";
	public final static String APP = "/Users/jamesgould/Documents/Bitbucket/GlobalRadioTechTest/global-recruitment-assessment.apk";
	public final static String AVD = "Nexus_4_API_22";
	public final static String AUTOMATION_NAME = "Appium";
	public final static String APP_ACTIVITY = "com.thisisglobal.avocado.main.MainActivity";
	
//	Home screen
	
	@FindBy(id="com.thisisglobal.player.choice.uat:id/toolbar") public static AndroidElement homescreentitle;
	
	@AndroidFindBy(accessibility="Play") public static AndroidElement playbutton;
	
	@AndroidFindBy(accessibility="Stop") public static AndroidElement stopbutton;
	
}