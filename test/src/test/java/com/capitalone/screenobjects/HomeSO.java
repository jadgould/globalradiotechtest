package com.capitalone.screenobjects;

import io.appium.java_client.android.AndroidElement;;

public class HomeSO extends screenobject {
	
	public Boolean exists() {
		return checkElementDisplayed(homescreentitle);
	}

	public void tap_play_button() {
		tap_button(playbutton);
	}

	public Boolean stop_button_displayed() {
		return checkElementDisplayed(stopbutton);
	}
	
}